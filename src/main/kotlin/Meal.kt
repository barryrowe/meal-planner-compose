data class Meal(
    val title: String,
    val mealType: String,
    val effort: String,
    val ingredients: String,
    val link: String?
)