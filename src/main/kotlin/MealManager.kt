import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.mutableStateMapOf

class MealManager {
    var allMeals = mutableStateListOf<Meal>()
        private set
    var mealPlan = mutableStateMapOf<MealType, List<Meal>>()
        private set

    fun setMeals(meals: List<Meal>) {
        allMeals.clear()
        allMeals.addAll(meals)
    }

    fun generateMealPlan() {
        mealPlan.clear()
        mealPlan.putAll(getMealsForWeek())
    }

    fun replaceMeal(key: MealType, index: Int, meal: Meal) {
        val meals = getMealsForMealType(key);
        val currentPlan = mealPlan[key]!!

        val option = meals
            .filter { opt -> currentPlan.none { it == opt } ?: false }
            .shuffled()
            .firstOrNull() ?: meals.first { it != meal }

        val newPlan = currentPlan.toMutableList()
        newPlan[index] = option
        mealPlan[key] = newPlan
    }

    private fun getMealsForMealType(mealType: MealType): List<Meal> {
        return allMeals.filter { it.mealType.contains(mealType.key) }
    }

    private fun getMealsForWeek(): Map<MealType, List<Meal>> {
        val allBreakfasts = getMealsForMealType(MealType.Breakfast)
        val allLunch = getMealsForMealType(MealType.Lunch)
        val allDinner = getMealsForMealType(MealType.Dinner)
        val breakfasts = allBreakfasts.randomizeCount(7);
        val lunches = allLunch.randomizeCount(7);
        val dinner = allDinner.randomizeCount(7);

        return mapOf(
            MealType.Breakfast to breakfasts,
            MealType.Lunch to lunches,
            MealType.Dinner to dinner
        )
    }

    private fun <T> List<T>.randomizeCount(count: Int) = when {
        this.size >= count -> this.shuffled().take(count)
        else -> 0.until(count).map { this.shuffled().first() }
    }
}