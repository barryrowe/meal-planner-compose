// Copyright 2000-2021 JetBrains s.r.o. and contributors. Use of this source code is governed by the Apache 2.0 license that can be found in the LICENSE file.
import androidx.compose.desktop.DesktopMaterialTheme
import androidx.compose.desktop.ui.tooling.preview.Preview
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.awt.ComposeWindow
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Window
import androidx.compose.ui.window.application
import com.github.doyaaaaaken.kotlincsv.dsl.csvReader
import io.blackmo18.kotlin.grass.dsl.grass
import java.awt.FileDialog
import java.io.File

@Composable
@Preview
fun App(composeWindow: ComposeWindow, mealMan: MealManager?) {
    val mealManager = remember { mealMan ?: MealManager() }

    DesktopMaterialTheme {
        Column {
            Row {
                Button(onClick = {
                    val fileset = openFileDialog(
                        window = composeWindow,
                        title =  "Choose Meals CSV",
                        allowedExtensions = listOf(".csv"),
                        allowMultiSelection = false
                    )
                    val meals = processFileset(fileSet = fileset)
                    mealManager.setMeals(meals)
                }) {
                    Text("Open Meal List")
                }

                Button(onClick = {
                        mealManager.generateMealPlan()
                    },
                    enabled = !mealManager.allMeals.isEmpty()
                ) {
                    Text("Generate Meal Plan")
                }
            }
//            Row(modifier = Modifier
//                .fillMaxWidth()
//                .padding(PaddingValues(vertical = 5.dp))
//            ){
//              Text("Meal Name", modifier = Modifier.weight(1f))
//              Text("MealType", modifier = Modifier.weight(1f))
//              Text("Effort", modifier = Modifier.weight(1f))
//              Text("Ingredients", modifier = Modifier.weight(1f))
//            }
//
//            mealManager.allMeals.forEach { meal ->
//                Row (modifier = Modifier.fillMaxWidth()){
//                    Text(meal.title, modifier = Modifier.weight(1f))
//                    Text(meal.mealType, modifier = Modifier.weight(1f))
//                    Text(meal.effort, modifier = Modifier.weight(1f))
//                    Text(meal.ingredients, modifier = Modifier.weight(1f))
//                }
//            }

            if (mealManager.mealPlan.isNotEmpty()) {
                Row(modifier = Modifier.fillMaxWidth()) {
                    Text("-------------------------")
                }
                mealManager.mealPlan.entries.sortedBy { it.key }.forEach { (key, meals) ->
                    Row(modifier = Modifier
                        .fillMaxWidth()
                        .padding(PaddingValues(vertical = 5.dp))
                    ) {
                        Text("***${key.name}:")
                    }
                    Row(modifier = Modifier.fillMaxWidth()) {
                        meals.forEachIndexed { index, meal ->
                            ClickableText(
                                modifier = Modifier.weight(1f),
                                text = AnnotatedString(meal.title),
                                onClick = { mealManager.replaceMeal(key, index, meal) }
                            )
                        }
                    }
                }
            }
        }

    }
}

fun main(args: Array<String>) = application {

    var mealManager: MealManager? = null;
    if (args.isNotEmpty()) {
        try {
            val file = File(args[0])
            mealManager = MealManager()
            val meals = processFileset(setOf(file))
            mealManager.setMeals(meals)
            mealManager.generateMealPlan()
        } catch (e: Exception) {
            // Do nothing
        }
    }
    Window(onCloseRequest = ::exitApplication) {
        App(composeWindow = this.window, mealManager)
    }
}

private fun openFileDialog(window: ComposeWindow, title: String, allowedExtensions: List<String>, allowMultiSelection: Boolean = true): Set<File> {
    return FileDialog(window, title, FileDialog.LOAD).apply {
        isMultipleMode = allowMultiSelection

        // windows
        file = allowedExtensions.joinToString(";") { "*$it" } // e.g. '*.jpg'

        // linux
        setFilenameFilter { _, name ->
            allowedExtensions.any {
                name.endsWith(it)
            }
        }

        isVisible = true
    }.files.toSet()
}

@OptIn(ExperimentalStdlibApi::class)
private fun processFileset(fileSet: Set<File>): List<Meal> {
    return if (fileSet.isEmpty()) {
        emptyList()
    } else {
        val contents = csvReader().readAllWithHeader(fileSet.first())
        grass<Meal>().harvest(contents)
    }
}
