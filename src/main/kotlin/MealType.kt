import androidx.compose.ui.text.intl.Locale
import androidx.compose.ui.text.toLowerCase

enum class MealType() {
    Breakfast(),
    Lunch(),
    Dinner();

    val key = name.toLowerCase(Locale.current)
}